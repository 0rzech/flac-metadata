defmodule FlacData do
  alias FlacData.{
    ParseError,
    Parser
  }

  @type t :: FlacData.Stream.t()

  @default_metadata_byte_size 3 * 1024 * 1024
  @default_metadata_real_order true

  @spec read(Path.t(), Keyword.t()) ::
          {:ok, t()}
          | {:error, ParseError.kind(), pos_integer()}
          | {:error, File.posix()}
          | {:error, Parser.error()}

  def read(file_path, params \\ [])
      when is_list(params) do
    case File.open(file_path, [:read]) do
      {:ok, file} ->
        load_and_parse(file, params)

      error ->
        error
    end
  end

  @spec parse(binary(), Keyword.t()) ::
          {:ok, t()}
          | {:error, ParseError.kind(), pos_integer()}
          | {:error, Parser.error()}

  def parse(binary, params \\ []) when is_binary(binary) and is_list(params) do
    Parser.parse!(binary, Keyword.get(params, :real_order, @default_metadata_real_order))
  rescue
    e in ParseError ->
      {:error, e.type, e.last_valid_bit}
  end

  defp load_and_parse(file, params) do
    file
    |> IO.binread(Keyword.get(params, :limit, @default_metadata_byte_size))
    |> Parser.parse!(Keyword.get(params, :real_order, @default_metadata_real_order))
  rescue
    e in ParseError ->
      {:error, e.type, e.last_valid_bit}
  after
    File.close(file)
  end
end
