defmodule Mix.Tasks.Project.Setup do
  use Mix.Task

  @shortdoc "Sets git hooks up"

  @spec run(term()) :: :ok

  @impl Mix.Task
  def run(_) do
    git_cmd = ~w(git config --local core.hooksPath '.githooks')
    Mix.shell().info(["Executing" | git_cmd] |> Enum.join(" "))
    Mix.Tasks.Cmd.run(git_cmd)
  end
end
