defmodule FlacData.VorbisComment do
  alias FlacData.ParseError

  @type t :: %__MODULE__{
          vendor: String.t(),
          user_comments: list({String.t(), String.t()})
        }
  @enforce_keys [:vendor, :user_comments]
  defstruct [:vendor, :user_comments]

  @length_size 32

  @spec parse!(nonempty_bitstring(), pos_integer()) :: {t(), pos_integer(), bitstring()}

  def parse!(bitstring, last_good_bit)
      when is_bitstring(bitstring) and is_integer(last_good_bit) do
    {vendor, vendor_bit_count, rest} = parse_vendor!(bitstring, last_good_bit)

    {comments, comments_bit_count, rest} =
      parse_user_comments!(rest, last_good_bit + vendor_bit_count)

    {
      %__MODULE__{
        vendor: vendor,
        user_comments: comments |> Enum.reverse()
      },
      vendor_bit_count + comments_bit_count,
      rest
    }
  end

  defp parse_vendor!(bitstring, last_good_bit) do
    case bitstring do
      <<
        length::little-size(@length_size),
        vendor::bytes-size(length),
        rest::bits
      >> ->
        {vendor, @length_size + length * 8, rest}

      _ ->
        raise ParseError,
          type: :malformed_vorbis_comment_block_vendor,
          last_valid_bit: last_good_bit
    end
  end

  defp parse_user_comments!(bitstring, last_good_bit) do
    case bitstring do
      <<
        count::little-size(@length_size),
        rest::bits
      >> ->
        parse_user_comments!(rest, count, last_good_bit + @length_size, [], @length_size)

      _ ->
        raise ParseError,
          type: :malformed_vorbis_comment_block_user_comment_length,
          last_valid_bit: last_good_bit
    end
  end

  defp parse_user_comments!(bitstring, 0, _, comments, total_bit_count),
    do: {comments, total_bit_count, bitstring}

  defp parse_user_comments!(bitstring, count, last_good_bit, comments, total_bit_count) do
    case bitstring do
      <<
        length::little-size(@length_size),
        comment::bytes-size(length),
        rest::bits
      >> ->
        bits_read = @length_size + length * 8

        [name, value] = String.split(comment, "=")

        parse_user_comments!(
          rest,
          count - 1,
          last_good_bit + bits_read,
          [{name, value} | comments],
          total_bit_count + bits_read
        )

      _ ->
        raise ParseError,
          type: :malformed_vorbis_comment_block_user_comment,
          last_valid_bit: last_good_bit
    end
  end
end
