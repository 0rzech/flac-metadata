defmodule FlacData.Unknown do
  alias FlacData.ParseError

  @type t :: %__MODULE__{type: pos_integer(), data: bitstring()}
  @enforce_keys [:type, :data]
  defstruct [:type, :data]

  @spec parse!(nonempty_bitstring(), non_neg_integer(), pos_integer(), pos_integer()) ::
          {t(), non_neg_integer(), bitstring()}

  def parse!(bitstring, length, type, last_good_bit)
      when is_bitstring(bitstring) and is_integer(length) and is_integer(last_good_bit) do
    case bitstring do
      <<data::bits-size(length), rest::bits>> ->
        {%__MODULE__{type: type, data: data}, length, rest}

      _ ->
        raise ParseError,
          type: :malformed_padding_block,
          last_valid_bit: last_good_bit
    end
  end
end
