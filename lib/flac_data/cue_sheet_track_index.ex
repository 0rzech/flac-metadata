defmodule FlacData.CueSheet.Track.Index do
  alias FlacData.ParseError

  @type t :: %__MODULE__{
          offset: non_neg_integer(),
          number: non_neg_integer()
        }
  @enforce_keys [:offset, :number]
  defstruct [:offset, :number]

  @offset_size 64
  @number_size 8
  @reserved_bits_size 3 * 8
  @size @offset_size + @number_size + @reserved_bits_size

  @spec parse!(nonempty_bitstring(), pos_integer()) :: {t(), pos_integer(), bitstring()}

  def parse!(bitstring, last_good_bit)
      when is_bitstring(bitstring) and is_integer(last_good_bit) do
    case bitstring do
      <<
        offset::size(@offset_size),
        number::size(@number_size),
        _::size(@reserved_bits_size),
        rest::bits
      >> ->
        {
          %__MODULE__{
            offset: offset,
            number: number
          },
          @size,
          rest
        }

      _ ->
        raise ParseError,
          type: :malformed_cue_sheet_track_index_block,
          last_valid_bit: last_good_bit
    end
  end
end
