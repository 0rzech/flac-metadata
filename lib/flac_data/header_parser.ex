defmodule FlacData.HeaderParser do
  alias FlacData.Header

  @type success :: {Header.t(), pos_integer(), bitstring()}
  @type failure :: {:malformed_header_block, non_neg_integer()}

  @spec parse(nonempty_bitstring(), pos_integer()) :: {:ok, success()} | {:error, failure()}

  def parse(bitstring, last_good_bit)
      when is_bitstring(bitstring) and is_integer(last_good_bit) do
    is_last_size = Header.is_last_size()
    type_size = Header.type_size()
    length_size = Header.length_size()

    with <<
           is_last::size(is_last_size),
           type::size(type_size),
           metadata_length::size(length_size),
           rest::bits
         >> <- bitstring,
         {:ok, header} <- Header.new(is_last == 1, type |> to_atom, metadata_length * 8) do
      {:ok, {header, Header.size(), rest}}
    else
      _ ->
        {:error, {:malformed_header_block, last_good_bit}}
    end
  end

  defp to_atom(type) when type in 0..127 do
    case type do
      0 -> :stream_info
      1 -> :padding
      2 -> :application
      3 -> :seek_table
      4 -> :vorbis_comment
      5 -> :cue_sheet
      6 -> :picture
      127 -> :invalid
      reserved -> reserved
    end
  end
end
