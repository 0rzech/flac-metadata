defmodule FlacData.Stream do
  alias FlacData.{
    Application,
    CueSheet,
    Header,
    Padding,
    Picture,
    SeekTable,
    StreamInfo,
    Unknown,
    VorbisComment
  }

  @type data_block ::
          Application.t()
          | CueSheet.t()
          | Padding.t()
          | Picture.t()
          | SeekTable.t()
          | StreamInfo.t()
          | Unknown.t()
          | VorbisComment.t()
  @type t :: %__MODULE__{
          blocks: nonempty_list({Header.t(), data_block()}),
          parsed_bit_count: pos_integer(),
          unknown_block_count: non_neg_integer()
        }
  @enforce_keys [:blocks, :parsed_bit_count, :unknown_block_count]
  defstruct [:blocks, :parsed_bit_count, :unknown_block_count]
end
