defmodule FlacData.CueSheet do
  alias FlacData.CueSheet.Track
  alias FlacData.ParseError

  @type t :: %__MODULE__{
          catalog_number: String.t(),
          lead_in_sample_count: non_neg_integer(),
          is_cd: boolean(),
          tracks: nonempty_list(Track.t())
        }
  @enforce_keys [:catalog_number, :lead_in_sample_count, :is_cd, :tracks]
  defstruct [:catalog_number, :lead_in_sample_count, :is_cd, :tracks]

  @catalog_number_size 128 * 8
  @lead_in_sample_count_size 64
  @is_cd_size 1
  @reserved_bits_size 7 + 258 * 8
  @track_count_size 8
  @size @catalog_number_size + @lead_in_sample_count_size + @is_cd_size + @reserved_bits_size +
          @track_count_size

  @spec parse!(nonempty_bitstring(), pos_integer()) :: {t(), pos_integer(), bitstring()}

  def parse!(bitstring, last_good_bit)
      when is_bitstring(bitstring) and is_integer(last_good_bit) do
    case bitstring do
      <<
        catalog_number::bits-size(@catalog_number_size),
        lead_in_sample_count::size(@lead_in_sample_count_size),
        is_cd::size(@is_cd_size),
        _::size(@reserved_bits_size),
        track_count::size(@track_count_size),
        rest::bits
      >> ->
        {tracks, bit_count, rest} = parse_tracks!(rest, track_count, last_good_bit + @size, [], 0)

        {
          %__MODULE__{
            catalog_number: catalog_number,
            lead_in_sample_count: lead_in_sample_count,
            is_cd: is_cd == 1,
            tracks: tracks |> Enum.reverse()
          },
          bit_count + @size,
          rest
        }

      _ ->
        raise ParseError,
          type: :malformed_cue_sheet_block,
          last_valid_bit: last_good_bit
    end
  end

  defp parse_tracks!(bitstring, 0, _, tracks, total_bit_count),
    do: {tracks, total_bit_count, bitstring}

  defp parse_tracks!(bitstring, count, last_good_bit, tracks, total_bit_count) do
    {track, bit_count, rest} = Track.parse!(bitstring, last_good_bit)

    parse_tracks!(
      rest,
      count - 1,
      last_good_bit + bit_count,
      [track | tracks],
      total_bit_count + bit_count
    )
  end
end
