defmodule FlacData.Parser do
  alias FlacData.{
    Application,
    CueSheet,
    ParseError,
    Header,
    HeaderParser,
    Padding,
    Picture,
    SeekTable,
    Stream,
    StreamInfo,
    Unknown,
    VorbisComment
  }

  @type error :: :empty_file | :not_a_flac | term()

  @stream_marker "fLaC"
  @stream_marker_size bit_size(@stream_marker)

  @spec parse!(binary() | :eof | {:error, term()}, boolean()) ::
          {:ok, FlacData.Stream.t()} | {:error, error() | term()}

  def parse!(:eof, _), do: {:error, :empty_file}
  def parse!({:error, _} = error, _), do: error

  def parse!(binary, real_order) when is_binary(binary) and is_boolean(real_order) do
    case binary do
      <<@stream_marker, rest::binary>> ->
        metadata = parse_metadata!(rest, @stream_marker_size)

        if real_order do
          {:ok, update_in(metadata.blocks, &Enum.reverse/1)}
        else
          {:ok, metadata}
        end

      _ ->
        {:error, :not_a_flac}
    end
  end

  defp parse_metadata!(bitstring, current_bit_count)
       when is_bitstring(bitstring) and is_integer(current_bit_count) do
    with {:ok, {%Header{type: :stream_info} = header, header_bit_count, rest}} <-
           HeaderParser.parse(bitstring, current_bit_count),
         {data, bit_count, rest} = StreamInfo.parse!(rest, current_bit_count) do
      metadata = %Stream{
        blocks: [{header, data}],
        parsed_bit_count: bit_count + header_bit_count + current_bit_count,
        unknown_block_count: 0
      }

      if header.is_last do
        metadata
      else
        parse_metadata!(rest, metadata)
      end
    else
      _ ->
        raise ParseError,
          type: :first_header_block_not_stream_info,
          last_valid_bit: current_bit_count
    end
  end

  defp parse_metadata!(bitstring, %Stream{parsed_bit_count: current_bit_count} = metadata)
       when is_bitstring(bitstring) do
    with {:ok, header} <- HeaderParser.parse(bitstring, current_bit_count),
         {header, data, bit_count, rest} <- parse_data!(header, current_bit_count) do
      metadata = prepend(metadata, header, data, bit_count)

      if header.is_last do
        metadata
      else
        parse_metadata!(rest, metadata)
      end
    else
      {:error, {type, last_good_bit}} ->
        raise ParseError,
          type: type,
          last_valid_bit: last_good_bit
    end
  end

  defp parse_data!(header, current_bit_count) do
    case header do
      {%Header{type: :padding} = header, header_bit_count, rest} ->
        Padding.parse!(rest, header.metadata_length, current_bit_count)
        |> join_with(header, header_bit_count)

      {%Header{type: :application} = header, header_bit_count, rest} ->
        Application.parse!(rest, header.metadata_length, current_bit_count)
        |> join_with(header, header_bit_count)

      {%Header{type: :seek_table} = header, header_bit_count, rest} ->
        SeekTable.parse!(rest, header.metadata_length, current_bit_count)
        |> join_with(header, header_bit_count)

      {%Header{type: :vorbis_comment} = header, header_bit_count, rest} ->
        VorbisComment.parse!(rest, current_bit_count)
        |> join_with(header, header_bit_count)

      {%Header{type: :cue_sheet} = header, header_bit_count, rest} ->
        CueSheet.parse!(rest, current_bit_count)
        |> join_with(header, header_bit_count)

      {%Header{type: :picture} = header, header_bit_count, rest} ->
        Picture.parse!(rest, current_bit_count)
        |> join_with(header, header_bit_count)

      {%Header{} = header, header_bit_count, rest} ->
        Unknown.parse!(rest, header.metadata_length, header.type, current_bit_count)
        |> join_with(header, header_bit_count)
    end
  end

  defp join_with({data, bit_count, rest}, header, header_bit_count),
    do: {header, data, bit_count + header_bit_count, rest}

  defp prepend(%Stream{} = metadata, %Header{} = header, block, bit_count) do
    metadata = %Stream{
      metadata
      | blocks: [{header, block} | metadata.blocks],
        parsed_bit_count: metadata.parsed_bit_count + bit_count
    }

    case block do
      %Unknown{} ->
        update_in(metadata.unknown_block_count, &(&1 + 1))

      _ ->
        metadata
    end
  end
end
