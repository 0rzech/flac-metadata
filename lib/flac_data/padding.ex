defmodule FlacData.Padding do
  alias FlacData.ParseError

  @type t :: %__MODULE__{size: pos_integer()}
  @enforce_keys [:size]
  defstruct [:size]

  @spec parse!(nonempty_bitstring(), non_neg_integer(), pos_integer()) ::
          {t(), non_neg_integer(), bitstring()}

  def parse!(bitstring, length, last_good_bit)
      when is_bitstring(bitstring) and is_integer(length) and is_integer(last_good_bit) do
    case bitstring do
      <<0::size(length), rest::bits>> ->
        {%__MODULE__{size: length}, length, rest}

      _ ->
        raise ParseError,
          type: :malformed_padding_block,
          last_valid_bit: last_good_bit
    end
  end
end
