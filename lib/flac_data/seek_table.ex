defmodule FlacData.SeekTable do
  alias FlacData.SeekPoint

  @type t :: nonempty_list(SeekPoint.t())

  @spec parse!(nonempty_bitstring(), non_neg_integer(), pos_integer()) ::
          {t(), pos_integer(), bitstring()}

  def parse!(bitstring, length, last_good_bit)
      when is_bitstring(bitstring) and is_integer(length) and is_integer(last_good_bit) do
    parse!(bitstring, length, last_good_bit, [], 0)
  end

  defp parse!(bitstring, 0, _, seekpoints, total_bit_count),
    do: {seekpoints |> Enum.reverse(), total_bit_count, bitstring}

  defp parse!(bitstring, length, last_good_bit, seekpoints, total_bit_count) do
    {data, bit_count, rest} = SeekPoint.parse!(bitstring, last_good_bit)

    parse!(
      rest,
      length - bit_count,
      last_good_bit + bit_count,
      [data | seekpoints],
      total_bit_count + bit_count
    )
  end
end

defmodule FlacData.SeekPoint do
  alias FlacData.ParseError

  @type t :: %__MODULE__{
          target_frame_first_sample_number: non_neg_integer() | :placeholder,
          first_to_target_frame_offset: non_neg_integer() | :undefined,
          target_frame_sample_count: non_neg_integer() | :undefined
        }
  @enforce_keys [
    :target_frame_first_sample_number,
    :first_to_target_frame_offset,
    :target_frame_sample_count
  ]
  defstruct [
    :target_frame_first_sample_number,
    :first_to_target_frame_offset,
    :target_frame_sample_count
  ]

  @first_sample_number_size 64
  @offset_size 64
  @count_size 16
  @size @first_sample_number_size + @offset_size + @count_size

  @spec parse!(nonempty_bitstring(), pos_integer()) :: {t(), pos_integer(), bitstring()}

  def parse!(bitstring, last_good_bit)
      when is_bitstring(bitstring) and is_integer(last_good_bit) do
    case bitstring do
      <<
        0xFFFFFFFFFFFFFFFF::size(@first_sample_number_size),
        _::size(@offset_size),
        _::size(@count_size),
        rest::bits
      >> ->
        {
          %__MODULE__{
            target_frame_first_sample_number: :placeholder,
            first_to_target_frame_offset: :undefined,
            target_frame_sample_count: :undefined
          },
          @size,
          rest
        }

      <<
        first_sample_number::size(@first_sample_number_size),
        offset::size(@offset_size),
        count::size(@count_size),
        rest::bits
      >> ->
        {
          %__MODULE__{
            target_frame_first_sample_number: first_sample_number,
            first_to_target_frame_offset: offset,
            target_frame_sample_count: count
          },
          @size,
          rest
        }

      _ ->
        raise ParseError,
          type: :malformed_seekpoint_block,
          last_valid_bit: last_good_bit
    end
  end
end
