defmodule FlacData.CueSheet.Track do
  alias FlacData.CueSheet.Track.Index
  alias FlacData.ParseError

  @type t :: %__MODULE__{
          offset: non_neg_integer(),
          number: pos_integer(),
          isrc: String.t(),
          is_audio: boolean(),
          has_pre_emphasis: boolean(),
          indices: nonempty_list(Index.t())
        }
  @enforce_keys [:offset, :number, :isrc, :is_audio, :has_pre_emphasis, :indices]
  defstruct [:offset, :number, :isrc, :is_audio, :has_pre_emphasis, :indices]

  @offset_size 64
  @number_size 8
  @isrc_size 12 * 8
  @is_audio_size 1
  @pre_emphasis_size 1
  @reserved_bits_size 6 + 13 * 8
  @index_count_size 8
  @size @offset_size + @number_size + @isrc_size + @is_audio_size + @pre_emphasis_size +
          @reserved_bits_size + @index_count_size

  @spec parse!(nonempty_bitstring(), pos_integer()) :: {t(), pos_integer(), bitstring()}

  def parse!(bitstring, last_good_bit)
      when is_bitstring(bitstring) and is_integer(last_good_bit) do
    case bitstring do
      <<
        offset::size(@offset_size),
        number::size(@number_size),
        isrc::bytes-size(@isrc_size),
        is_audio::size(@is_audio_size),
        pre_emphasis::size(@pre_emphasis_size),
        _::size(@reserved_bits_size),
        index_count::size(@index_count_size),
        rest::bits
      >> ->
        {indices, bit_count, rest} =
          parse_indices!(rest, index_count, last_good_bit + @size, [], 0)

        {
          %__MODULE__{
            offset: offset,
            number: number,
            isrc: isrc,
            is_audio: is_audio == 1,
            has_pre_emphasis: pre_emphasis == 1,
            indices: indices |> Enum.reverse()
          },
          bit_count + @size,
          rest
        }

      _ ->
        raise ParseError,
          type: :malformed_cue_sheet_track_block,
          last_valid_bit: last_good_bit
    end
  end

  defp parse_indices!(bitstring, 0, _, indices, total_bit_count),
    do: {indices, total_bit_count, bitstring}

  defp parse_indices!(bitstring, count, last_good_bit, indices, total_bit_count) do
    {index, bit_count, rest} = Index.parse!(bitstring, last_good_bit)

    parse_indices!(
      rest,
      count - 1,
      last_good_bit + bit_count,
      [index | indices],
      total_bit_count + bit_count
    )
  end
end
