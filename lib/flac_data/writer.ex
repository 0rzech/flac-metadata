defprotocol FlacData.Writer do
  @type stream_fragment ::
          FlacData.Application.t()
          | FlacData.CueSheet.t()
          | FlacData.Header.t()
          | FlacData.Padding.t()
          | FlacData.Picture.t()
          | FlacData.SeekTable.t()
          | FlacData.StreamInfo.t()
          | FlacData.Unknown.t()
          | FlacData.VorbisComment.t()

  @spec to_bitstring(stream_fragment()) :: bitstring()
  def to_bitstring(data)
end
