defmodule FlacData.ParseError do
  defexception [:type, :last_valid_bit]

  @type kind ::
          :malformed_application_block
          | :malformed_cue_sheet_block
          | :malformed_cue_sheet_track_block
          | :malformed_cue_sheet_track_index_block
          | :malformed_header_block
          | :malformed_padding_block
          | :malformed_picture_block
          | :malformed_seekpoint_block
          | :malformed_stream_info_block
          | :malformed_vorbis_comment_block_vendor_block
          | :malformed_vorbis_comment_block_user_comment
          | :malformed_vorbis_comment_block_user_comment_length

  @type t :: %__MODULE__{
          type: kind(),
          last_valid_bit: pos_integer()
        }

  @spec message(t()) :: String.t()

  @impl Exception
  def message(%__MODULE__{} = error) do
    type =
      error.type
      |> Atom.to_string()
      |> String.capitalize()
      |> String.replace("_", " ")

    "#{type}\nLast bit of fully parsed block: #{error.last_valid_bit}"
  end
end

defimpl String.Chars, for: FlacData.ParseError do
  alias FlacData.ParseError

  @spec to_string(ParseError.t()) :: String.t()

  def to_string(%ParseError{} = error), do: error |> Exception.message()
end
