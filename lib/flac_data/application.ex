defmodule FlacData.Application do
  alias FlacData.ParseError

  @type t :: %__MODULE__{
          id: String.t(),
          data: binary()
        }
  @enforce_keys [:id, :data]
  defstruct [:id, :data]

  @id_size 32

  @spec parse!(nonempty_bitstring(), non_neg_integer(), pos_integer()) ::
          {t(), pos_integer(), bitstring()}

  def parse!(bitstring, length, last_good_bit)
      when is_bitstring(bitstring) and is_integer(length) and is_integer(last_good_bit) do
    case bitstring do
      <<
        id::bytes-size(@id_size),
        data::bytes-size(length - @id_size),
        rest::bits
      >> ->
        {
          %__MODULE__{
            id: id,
            data: data
          },
          length,
          rest
        }

      _ ->
        raise ParseError,
          type: :malformed_application_block,
          last_valid_bit: last_good_bit
    end
  end
end
