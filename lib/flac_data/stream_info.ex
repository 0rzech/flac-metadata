defmodule FlacData.StreamInfo do
  alias FlacData.ParseError

  @type t :: %__MODULE__{
          minimum_block_size: non_neg_integer(),
          maximum_block_size: non_neg_integer(),
          minimum_frame_size: pos_integer() | :unknown,
          maximum_frame_size: pos_integer() | :unknown,
          sample_rate: pos_integer() | :invalid,
          number_of_channels: pos_integer(),
          bits_per_sample: pos_integer() | :invalid,
          total_stream_samples: pos_integer() | :unknown,
          checksum: String.t()
        }
  @enforce_keys [
    :minimum_block_size,
    :maximum_block_size,
    :minimum_frame_size,
    :maximum_frame_size,
    :sample_rate,
    :number_of_channels,
    :bits_per_sample,
    :total_stream_samples,
    :checksum
  ]
  defstruct [
    :minimum_block_size,
    :maximum_block_size,
    :minimum_frame_size,
    :maximum_frame_size,
    :sample_rate,
    :number_of_channels,
    :bits_per_sample,
    :total_stream_samples,
    :checksum
  ]

  @block_size 16
  @frame_size 24
  @sample_rate_size 20
  @channels_size 3
  @bits_per_sample_size 5
  @total_stream_samples_size 36
  @md5_size 128
  @size (@block_size + @frame_size) * 2 + @sample_rate_size + @channels_size +
          @bits_per_sample_size + @total_stream_samples_size + @md5_size

  @spec parse!(nonempty_bitstring(), pos_integer()) :: {t(), pos_integer(), bitstring()}

  def parse!(bitstring, last_good_bit)
      when is_bitstring(bitstring) and is_integer(last_good_bit) do
    case bitstring do
      <<
        minimum_block_size::size(@block_size),
        maximum_block_size::size(@block_size),
        minimum_frame_size::size(@frame_size),
        maximum_frame_size::size(@frame_size),
        sample_rate::size(@sample_rate_size),
        channels::size(@channels_size),
        bits_per_sample::size(@bits_per_sample_size),
        total_stream_samples::size(@total_stream_samples_size),
        md5::bytes-size(@md5_size)-unit(1),
        rest::bits
      >> ->
        {
          %__MODULE__{
            minimum_block_size: minimum_block_size,
            maximum_block_size: maximum_block_size,
            minimum_frame_size: minimum_frame_size |> or_unknown,
            maximum_frame_size: maximum_frame_size |> or_unknown,
            sample_rate: sample_rate |> or_invalid_sr,
            number_of_channels: channels + 1,
            bits_per_sample: bits_per_sample |> or_invalid_bps,
            total_stream_samples: total_stream_samples |> or_unknown,
            checksum: Base.encode16(md5) |> String.downcase()
          },
          @size,
          rest
        }

      _ ->
        raise ParseError,
          type: :malformed_stream_info_block,
          last_valid_bit: last_good_bit
    end
  end

  defp or_unknown(frame_size) when frame_size > 0, do: frame_size
  defp or_unknown(_), do: :unknown

  defp or_invalid_sr(sample_rate) when sample_rate in 1..655_350, do: sample_rate
  defp or_invalid_sr(_), do: :invalid

  defp or_invalid_bps(bits_per_sample) when bits_per_sample in 3..31, do: bits_per_sample + 1
  defp or_invalid_bps(_), do: :invalid
end
