defmodule FlacData.Header do
  @type type ::
          :stream_info
          | :padding
          | :application
          | :seek_table
          | :vorbis_comment
          | :cue_sheet
          | :picture
          | :invalid
          | 7..126
  @type t :: %__MODULE__{
          is_last: boolean(),
          type: type(),
          metadata_length: non_neg_integer()
        }
  @enforce_keys [:is_last, :type, :metadata_length]
  defstruct [:is_last, :type, :metadata_length]

  @expected_type_value """
  :application or :cue_sheet or :padding or :picture\
  or :seek_table or :stream_info or :vorbis_comment or 7..126\
  """

  @expected_metadata_length "0..16_777_215"

  @is_last_size 1
  @type_size 7
  @length_size 24
  @size @is_last_size + @type_size + @length_size

  @spec new(boolean(), type(), non_neg_integer()) :: {:ok, t()} | {:error, term()}

  def new(is_last, type, metadata_length) do
    with {:ok, type} <- validated_type(type),
         {:ok, length} <- validated_length(metadata_length) do
      {:ok,
       %__MODULE__{
         is_last: is_last,
         type: type,
         metadata_length: length
       }}
    else
      error -> error
    end
  end

  def is_last_size(), do: @is_last_size

  def type_size(), do: @type_size

  def length_size(), do: @length_size

  def size(), do: @size

  defp validated_type(type)
       when type in [
              :application,
              :cue_sheet,
              :padding,
              :picture,
              :seek_table,
              :stream_info,
              :vorbis_comment
            ] or
              type in 7..126,
       do: {:ok, type}

  defp validated_type(type), do: {:error, {:invalid_header_type, type, @expected_type_value}}

  defp validated_length(metadata_length) when metadata_length in 0..16_777_215,
    do: {:ok, metadata_length}

  defp validated_length(metadata_length),
    do: {:error, {:invalid_header_metadata_length, metadata_length, @expected_metadata_length}}
end
