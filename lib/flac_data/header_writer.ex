alias FlacData.{Header, Writer}

defimpl Writer, for: Header do
  @spec to_bitstring(Header.t()) :: nonempty_bitstring()

  @impl Writer
  def to_bitstring(%Header{} = header) do
    is_last = if header.is_last, do: 1, else: 0
    type = to_number(header.type)
    metadata_length = Integer.floor_div(header.metadata_length, 8)

    <<
      is_last::size(Header.is_last_size()),
      type::size(Header.type_size()),
      metadata_length::size(Header.length_size())
    >>
  end

  defp to_number(type) when is_atom(type) or is_integer(type) do
    case type do
      :stream_info -> 0
      :padding -> 1
      :application -> 2
      :seek_table -> 3
      :vorbis_comment -> 4
      :cue_sheet -> 5
      :picture -> 6
      type when type in 7..126 -> type
    end
  end
end
