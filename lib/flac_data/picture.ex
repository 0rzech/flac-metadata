defmodule FlacData.Picture do
  alias FlacData.ParseError

  @type t :: %__MODULE__{
          type: atom(),
          mime_type: String.t(),
          description: String.t(),
          width: non_neg_integer(),
          height: non_neg_integer(),
          color_depth: non_neg_integer(),
          color_count: pos_integer() | :non_indexed_picture,
          data: binary()
        }
  @enforce_keys [
    :type,
    :mime_type,
    :description,
    :width,
    :height,
    :color_depth,
    :color_count,
    :data
  ]
  defstruct [:type, :mime_type, :description, :width, :height, :color_depth, :color_count, :data]

  @type_size 32
  @mime_length_size 32
  @description_length_size 32
  @width_size 32
  @height_size 32
  @color_depth_size 32
  @color_count_size 32
  @data_length_size 32
  @size @type_size + @mime_length_size + @description_length_size + @width_size + @height_size +
          @color_depth_size + @color_count_size + @data_length_size

  @spec parse!(nonempty_bitstring(), pos_integer()) :: {t(), pos_integer(), bitstring()}

  def parse!(bitstring, last_good_bit)
      when is_bitstring(bitstring) and is_integer(last_good_bit) do
    case bitstring do
      <<
        type::size(@type_size),
        mime_length::size(@mime_length_size),
        mime_type::bytes-size(mime_length),
        description_length::size(@description_length_size),
        description::bytes-size(description_length),
        width::size(@width_size),
        height::size(@height_size),
        color_depth::size(@color_depth_size),
        color_count::size(@color_count_size),
        data_length::size(@data_length_size),
        data::bytes-size(data_length),
        rest::bits
      >> ->
        {
          %__MODULE__{
            type: type |> to_atom,
            mime_type: mime_type,
            description: description,
            width: width,
            height: height,
            color_depth: color_depth,
            color_count: color_count |> or_non_indexed,
            data: data
          },
          @size + (mime_length + description_length + data_length) * 8,
          rest
        }

      _ ->
        raise ParseError,
          type: :malformed_picture_block,
          last_valid_bit: last_good_bit
    end
  end

  defp to_atom(type) when type in 0..0xFFFFFFFF do
    case type do
      0 -> :other
      1 -> :icon
      2 -> :icon_other
      3 -> :front_cover
      4 -> :back_cover
      5 -> :leaflet
      6 -> :media
      7 -> :lead_artist_performer_soloist
      8 -> :artist_performer
      9 -> :conductor
      10 -> :band_orchestra
      11 -> :composer
      12 -> :lyricist_text_writer
      13 -> :recording_location
      14 -> :during_recording
      15 -> :during_performance
      16 -> :movie_video_screen_capture
      17 -> :a_bright_coloured_fish
      18 -> :illustration
      19 -> :band_artist_logotype
      20 -> :publisher_studio_logotype
      _ -> :reserved
    end
  end

  defp or_non_indexed(color_count) when color_count > 0, do: color_count
  defp or_non_indexed(_), do: :non_indexed_picture
end
