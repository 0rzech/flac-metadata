defmodule FlacData.MixProject do
  use Mix.Project

  @scm_url "https://gitlab.com/0rzech/flac-data"

  def project do
    [
      app: :flac_data,
      version: "0.1.0",
      elixir: "~> 1.15.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      dialyzer: dialyzer(),
      name: "Flac Data",
      package: package(),
      source_url: @scm_url,
      homepage: @scm_url,
      description: "Flac data handling library"
    ]
  end

  defp package do
    [
      maintainers: ["Piotr Orzechowski"],
      licenses: ["Apache-2.0"],
      links: %{"GitLab" => @scm_url},
      files: ~w(lib mix.exs README.md .formatter.exs)
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp dialyzer() do
    [
      flags: [:error_handling, :underspecs],
      plt_add_apps: [:mix]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dialyxir, "~> 1.4.2", only: [:dev, :test], runtime: false}
    ]
  end
end
