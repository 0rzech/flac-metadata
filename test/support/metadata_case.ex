defmodule FlacData.MetadataCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      alias FlacData.{
        Application,
        CueSheet,
        ParseError,
        Header,
        HeaderParser,
        Padding,
        Picture,
        SeekTable,
        Stream,
        StreamInfo,
        Unknown,
        WriteError,
        Writer
      }

      alias FlacData.Fixtures

      defmacrop assert_parse_error(value, opts \\ []) do
        quote do
          value = unquote(value)

          assert {:error, {type, last_valid_bit}} = value

          opts = unquote(opts)

          asserted_type = Keyword.get(opts, :type)
          if asserted_type, do: assert(type == asserted_type)

          asserted_last_valid_bit = Keyword.get(opts, :last_balid_bit)
          if asserted_last_valid_bit, do: assert(value == asserted_last_valid_bit)
        end
      end

      defmacrop assert_raise_parse_error(func, opts \\ []) do
        quote do
          opts = unquote(opts)

          try do
            unquote(func).()
            flunk("Expected exception #{ParseError} but nothing was raised")
          rescue
            exception in ParseError ->
              type = Keyword.get(opts, :type)
              if type, do: assert(exception.type == type)

              last_valid_bit = Keyword.get(opts, :last_valid_bit)
              if last_valid_bit, do: assert(exception.last_valid_bit == last_valid_bit)
          end
        end
      end

      defmacrop assert_write_error(value, opts \\ []) do
        quote do
          value = unquote(value)

          assert {:error, {type, value, expected_value}} = value

          opts = unquote(opts)

          asserted_type = Keyword.get(opts, :type)
          if asserted_type, do: assert(type == asserted_type)

          asserted_value = Keyword.get(opts, :value)
          if asserted_value, do: assert(value == asserted_value)

          asserted_expected_value = Keyword.get(opts, :expected_value)
          if asserted_expected_value, do: assert(expected_value == asserted_expected_value)

          asserted_expected_value_match = Keyword.get(opts, :expected_value_match)

          if asserted_expected_value_match,
            do: assert(expected_value =~ asserted_expected_value_match)
        end
      end
    end
  end
end
