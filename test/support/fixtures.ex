defmodule FlacData.Fixtures do
  alias FlacData.{Header, Writer}

  def header_struct(attrs \\ %{}) do
    default_header()
    |> struct(attrs)
  end

  def header_binary(attrs \\ %{}) do
    default_header()
    |> struct(attrs)
    |> Writer.to_bitstring()
  end

  defp default_header() do
    %Header{
      is_last: false,
      type: :stream_info,
      metadata_length: 256
    }
  end
end
