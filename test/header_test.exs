defmodule FlacData.HeaderTest do
  use FlacData.MetadataCase, async: true

  test "parses and writes header data" do
    # given
    input_header = Fixtures.header_struct()
    input_bitstring = Fixtures.header_binary()

    # when
    {:ok, {output_header, 32, _}} = HeaderParser.parse(input_bitstring, 0)
    output_bitstring = Writer.to_bitstring(output_header)

    # then
    assert input_header == output_header
    assert input_bitstring == output_bitstring
  end

  test "parse/2 rejects malformed bitstring" do
    # given
    input_bitstring = <<0>>
    last_valid_bit = 15

    # when
    result = HeaderParser.parse(input_bitstring, last_valid_bit)

    # then
    assert_parse_error(result,
      type: :malformed_header_block,
      last_valid_bit: last_valid_bit
    )
  end

  describe "new/3" do
    test "passes valid header" do
      # given
      header = Fixtures.header_struct(is_last: true)

      # when
      result = Header.new(header.is_last, header.type, header.metadata_length)

      # then
      assert {:ok, %Header{} = header} == result
    end

    test "rejects invalid header type" do
      # given
      header = Fixtures.header_struct(type: 127)

      # when
      result = Header.new(header.is_last, header.type, header.metadata_length)

      # then
      assert_write_error(result,
        type: :invalid_header_type,
        value: header.type,
        # expected to match 7 atoms and range 7..126
        expected_value_match: ~r|^(:[acpsv].+){7}7\.\.126$|
      )
    end

    test "rejects invalid metadata length" do
      # given
      header = Fixtures.header_struct(metadata_length: -1)

      # when
      result = Header.new(header.is_last, header.type, header.metadata_length)

      # then
      assert_write_error(result,
        type: :invalid_header_metadata_length,
        value: header.metadata_length,
        expected_value: "0..16_777_215"
      )
    end
  end
end
